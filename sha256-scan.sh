#!/usr/bin/env bash 
#################### Safe header start #########################
# Created by: Silent Mobius AKA Alex M. Schapelle
# Purpose: sha256 sign on exe files
# Version: 0.1.0
# Date: 27.11.2023
# set -x
set -o errexit
set -o pipefail
# set -o
PROJECT=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SEEK_PATH="${1:-$PROJECT}"
SHA_TOOL=$(which sha256sum)
SEPERATOR="========================="
EXTENTION="${EXT:-*.exe}"
. /etc/os-release

#################### Safe header rnd #########################


function main(){
    
    dependency_check
    
    if [[ ${#} -le 0 ]];then
        usage
    fi

    deco '[+] Starting Scan For Exe Files'

        ARRAY_OF_EXE_FILES=$(find_exe_file $SEEK_PATH)

    deco '[+] Signing The Found Files'
    
        sign_exe_files "${ARRAY_OF_EXE_FILES[@]}"
    
    deco '[+] '
    }




function deco(){
    local IN=$1
    printf "#$SEPERATOR\n# %s\n#$SEPERATOR\n" "$IN"
    sleep 1
    }

function usage(){
    deco "
[!] Incorrect use:
[!] Synopsis: Sha256-scan tool is shell script for scaninig EXE file. 
[!] EXE can be .exe(default), .bin, .zip and so on
[!] EXE can be overriden with environment variable EXT, for example:
[>>]   export EXE='.bin'
[!]  To run the tool add execution permission to the script and pass the path to binary files
[!] For Example:
[>>] chmod +x $0
[>>] $0 /path/to/exe/files
          
    "
        exit 0
    }

function dependency_check(){
    if [[ ! -e $HOME/.config/sha256_tool_exists ]];then

        deco '[+] Checking Dependencies'
    
        if [[ ! -e $SHA_TOOL ]];then
            deco "[!] Dependency missing: $SHA_TOOL
                  [!] Please install $SHA_TOOL tool
                "
            return 1
        else
            touch $HOME/.config/sha256_tool_exists

        fi
      return 0
    fi
    }

function find_exe_file(){
    local IN="$1"
        SORTED_ARRAY_OF_EXE_FILES=$(find "$IN" -name "$EXTENTION"| sort)
        echo "${SORTED_ARRAY_OF_EXE_FILES[@]}"
    }

function sign_exe_files(){
    local IN="$1"

    for _file in ${IN[@]}
    # for File in "${ARRAY_OF_EXE_FILES[@]}"
        do  

            TMP_SHA_VALUE=$($SHA_TOOL $_file | awk '{print $1}')
                
            if [[ ! -e "${_file%.exe}.sha256" ]];then
                
                deco "[+] Generating  ${_file%.exe}.sha256"
                echo $TMP_SHA_VALUE > "${_file%.exe}.sha256"

            elif [[ -e "${_file%.exe}.sha256" ]] && [[ $(cat "${_file%.exe}.sha256") != "$TMP_SHA_VALUE" ]];then
                deco "[!] ${_file%.exe}.sha256 exists but $TMP_SHA_VALUE value do NOT match"
            
            else
                deco "[!] ${_file%.exe}.sha256 exists and $TMP_SHA_VALUE value match"
            fi
        done

    }

####
# Main
####
main "$@"
