# Waterfall task

- Write a shell script with bash
    - The script should calculates SHA256 for every `exe` file in given directory
    - Script needs to store the SHA256 value in file named after the `exe` that was changed
    - In case of existing sha file, skip
        - Do verify that existing value and checked value are the same

- Example:
    - file1.exe
    - file1.sha256